﻿using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;
using Infauto.Services.Interfaces;
using Infauto.Services.Services;

namespace Infauto.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/Vehicle")]
    public class VehicleController : ApiController
    {
        private readonly IVehicleService vehicleService;
        private readonly IUserService userService;

        //public VehicleController(IVehicleService vehicleService)
        //{
        //    this.vehicleService = vehicleService;
        //}

        public VehicleController()
        {
            vehicleService = new VehicleService();
            userService = new UserService();
        }

        [HttpGet]
        [Route("GetAllByUser/{id:int}")]
        public async Task<IEnumerable<VehicleDTO>> GetAllByUser(int id)
        {
            return await vehicleService.GetAllByUserAsync(id);
        }

        [HttpPost]
        [Route("Add")]
        public VehicleDTO Add(VehicleRequest request)
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            var userId = userService.GetUserId(principal.Identity.Name);
            return vehicleService.Add(request, userId);
        }

        [HttpPost]
        [Route("Update")]
        public VehicleDTO Update(UpdateVehicleRequest request)
        {
            return vehicleService.Update(request);
        }

        [HttpPost]
        [Route("Delete")]
        public IEnumerable<int> Delete(DeleteRequest request)
        {
            return vehicleService.Delete(request);
        }
    }
}