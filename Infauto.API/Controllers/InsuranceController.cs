﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;
using Infauto.Services.Interfaces;
using Infauto.Services.Services;

namespace Infauto.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/Insurance")]
    public class InsuranceController : ApiController
    {
        private readonly IInsuranceService insuranceService;

        //public InsuranceController(IInsuranceService insuranceService)
        //{
        //    this.insuranceService = insuranceService;
        //}

        public InsuranceController()
        {
            this.insuranceService = new InsuranceService();
        }

        [HttpGet]
        [Route("GetAll/{id}")]
        public async Task<IEnumerable<InsuranceDTO>> GetByUser(int id)
        {
            return await insuranceService.GetAllByUserAsync(id);
        }

        [HttpPost]
        [Route("Add")]
        public InsuranceDTO Add(InsuranceRequest request)
        {
            return insuranceService.Add(request);
        }

        [HttpPost]
        [Route("Update")]
        public InsuranceDTO Update(UpdateInsuranceRequest request)
        {
            return insuranceService.Update(request);
        }

        [HttpPost]
        [Route("Delete")]
        public IEnumerable<int> Delete(DeleteRequest request)
        {
            return insuranceService.Delete(request);
        } 
    }
}