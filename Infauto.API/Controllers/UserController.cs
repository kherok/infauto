﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;
using Infauto.Services.Interfaces;
using Infauto.Services.Services;

namespace Infauto.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        private readonly IUserService userService;

        //public UserController(IUserService userService)
        //{
        //    this.userService = userService;
        //}

        public UserController()
        {
            userService = new UserService();
        }

        [HttpGet]
        [Route("GetUser/{id}")]
        public async Task<UserDTO> GetUser(string id)
        {
            return await userService.GetUserAsync(id);
        }

        [HttpPost]
        [Route("Add")]
        public UserDTO Add(UserRequest request)
        {
            return userService.Add(request);
        }

        [HttpPost]
        [Route("Update")]
        public UserDTO Update(UpdateUserRequest request)
        {
            return userService.Update(request);
        }

        [HttpPost]
        [Route("Delete")]
        public IEnumerable<int> Delete(DeleteRequest request)
        {
            return userService.Delete(request);
        }
    }
}