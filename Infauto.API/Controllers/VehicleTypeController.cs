﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;
using Infauto.Services.Interfaces;
using Infauto.Services.Services;

namespace Infauto.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/VehicleType")]
    public class VehicleTypeController : ApiController
    {
        private readonly IVehicleTypeService vehicleTypeService;

        //public VehicleTypeController(IVehicleTypeService vehicleTypeService)
        //{
        //    this.vehicleTypeService = vehicleTypeService;
        //}

        public VehicleTypeController()
        {
            vehicleTypeService = new VehicleTypeService();
        }

        [HttpGet]
        [Route("GetAllByUser/{id}")]
        public async Task<IEnumerable<VehicleTypeDTO>> GetAllByUser(int id)
        {
            return await vehicleTypeService.GetAllByUserAsync(id);
        }

        [HttpPost]
        [Route("Add")]
        public VehicleTypeDTO Add(VehicleTypeRequest request)
        {
            return vehicleTypeService.Add(request);
        }

        [HttpPost]
        [Route("Update")]
        public VehicleTypeDTO Update(UpdateVehicleTypeRequest request)
        {
            return vehicleTypeService.Update(request);
        }

        [HttpPost]
        [Route("Delete")]
        public IEnumerable<int> Delete(DeleteRequest request)
        {
            return vehicleTypeService.Delete(request);
        }
    }
}