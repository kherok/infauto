﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;
using Infauto.Services.Interfaces;
using Infauto.Services.Services;

namespace Infauto.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/Refueling")]
    public class RefuelingController : ApiController
    {
        private readonly IRefuelingService refuelingService;

        //public RefuelingController(IRefuelingService refuelingService)
        //{
        //    this.refuelingService = refuelingService;
        //}

        public RefuelingController()
        {
            refuelingService = new RefuelingService();
        }

        [HttpGet]
        [Route("GetAllByUser/{id}")]
        public async Task<IEnumerable<RefuelingDTO>> GetAllByUser(int id)
        {
            return await refuelingService.GetAllByUserAsync(id);
        }

        [HttpGet]
        [Route("GetAllByVehicle/{id}")]
        public async Task<IEnumerable<RefuelingDTO>> GetAllByVehicle(int id)
        {
            return await refuelingService.GetAllByVehicleAsync(id);
        }

        [HttpPost]
        [Route("Add")]
        public RefuelingDTO Add(RefuelingRequest request)
        {
            return refuelingService.Add(request);
        }

        [HttpPost]
        [Route("Update")]
        public RefuelingDTO Update(UpdateRefuelingRequest request)
        {
            return refuelingService.Update(request);
        }

        [HttpPost]
        [Route("Delete")]
        public IEnumerable<int> Delete(DeleteRequest request)
        {
            return refuelingService.Delete(request);
        }
    }
}