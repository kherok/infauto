﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;
using Infauto.Services.Interfaces;
using Infauto.Services.Services;

namespace Infauto.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/Service")]
    public class ServiceController:ApiController
    {
        private readonly IServiceService serviceService;

        //public ServiceController(IServiceService serviceService)
        //{
        //    this.serviceService = serviceService;
        //}

        public ServiceController()
        {
            serviceService = new ServiceService();
        }

        [HttpGet]
        [Route("GetAllByUser/{id}")]
        public async Task<IEnumerable<ServiceDTO>> GetAllByUser(int id)
        {
            return await serviceService.GetAllByUserAsync(id);
        }

        [HttpGet]
        [Route("GetAllByVehicle/{id}")]
        public async Task<IEnumerable<ServiceDTO>> GetAllByVehicle(int id)
        {
            return await serviceService.GetAllByVehicleAsync(id);
        }

        [HttpPost]
        [Route("Add")]
        public ServiceDTO Add(ServiceRequest request)
        {
            return serviceService.Add(request);
        }

        [HttpPost]
        [Route("Update")]
        public ServiceDTO Update(UpdateServiceRequest request)
        {
            return serviceService.Update(request);
        }

        [HttpPost]
        [Route("Delete")]
        public IEnumerable<int> Delete(DeleteRequest request)
        {
            return serviceService.Delete(request);
        }
    }
}