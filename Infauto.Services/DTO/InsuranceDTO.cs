﻿using System;
using Infauto.DataAccess.Entities;

namespace Infauto.Services.DTO
{
    public class InsuranceDTO
    {
        public int Id { get; set; }
        public string InsuranceFrom { get; set; }
        public string InsuranceTo { get; set; }
        public double InsuranceCost { get; set; }
        public int VehicleId { get; set; }
        public string Vehicle { get; set; }
    }
}