﻿namespace Infauto.Services.DTO
{
    public class RefuelingDTO
    {
        public int Id { get; set; }
        public int MileageDifference { get; set; }
        public double FuelCost { get; set; }
        public int VehicleId { get; set; }
        public string Vehicle { get; set; }
    }
}