﻿using System.Collections.Generic;

namespace Infauto.Services.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public IEnumerable<VehicleDTO> Vehicles { get; set; }
    }
}