﻿using System.Collections.Generic;

namespace Infauto.Services.DTO
{
    public class VehicleTypeDTO
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string FuelType { get; set; }
        public virtual IEnumerable<VehicleDTO> Vehicles { get; set; } 
    }
}