﻿using System.Collections.Generic;

namespace Infauto.Services.DTO
{
    public class VehicleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Mileage { get; set; }
        public int HorsePower { get; set; }
        public int ProductionYear { get; set; }
        public int UserId { get; set; }
        public virtual UserDTO UserDto { get; set; }
        public int? VehicleTypeId { get; set; }
        public VehicleTypeDTO VehicleType { get; set; }
        public IEnumerable<RefuelingDTO> Refuelings { get; set; }
        public IEnumerable<InsuranceDTO> Insurances { get; set; }
        public IEnumerable<ServiceDTO> Services { get; set; }
    }
}