﻿namespace Infauto.Services.DTO.Contracts
{
    public class VehicleRequest
    {
        public string Name { get; set; }
        public int Mileage { get; set; }
        public int HorsePower { get; set; }
        public int ProductionYear { get; set; }
    }
}