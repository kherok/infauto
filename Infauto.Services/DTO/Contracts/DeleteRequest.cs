﻿using System.Collections.Generic;

namespace Infauto.Services.DTO.Contracts
{
    public class DeleteRequest
    {
        public IEnumerable<int> Ids { get; set; }
    }
}
