﻿namespace Infauto.Services.DTO.Contracts
{
    public class UpdateRefuelingRequest : RefuelingRequest
    {
        public int Id { get; set; }
    }
}