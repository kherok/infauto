﻿using Infauto.DataAccess.Entities;

namespace Infauto.Services.DTO.Contracts
{
    public class RefuelingRequest
    {
        public int MileageDifference { get; set; }
        public double FuelCost { get; set; }
        public int VehicleId { get; set; }
    }
}