﻿namespace Infauto.Services.DTO.Contracts
{
    public class UpdateUserRequest : UserRequest
    {
        public int Id { get; set; }
    }
}