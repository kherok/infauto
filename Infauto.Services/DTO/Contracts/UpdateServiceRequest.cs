﻿namespace Infauto.Services.DTO.Contracts
{
    public class UpdateServiceRequest :ServiceRequest
    {
        public int Id { get; set; }
    }
}