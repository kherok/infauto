﻿namespace Infauto.Services.DTO.Contracts
{
    public class ServiceRequest
    {
        public string ServiceDescription { get; set; }
        public double ServiceCost { get; set; }
        public int Mileage { get; set; }
        public bool IsCyclic { get; set; }
        public int? MileageCycle { get; set; }
        public int VehicleId { get; set; }
    }
}