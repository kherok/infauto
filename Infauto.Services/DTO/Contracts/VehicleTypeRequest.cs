﻿namespace Infauto.Services.DTO.Contracts
{
    public class VehicleTypeRequest
    {
        public string Type { get; set; }
        public string FuelType { get; set; }
    }
}