﻿namespace Infauto.Services.DTO.Contracts
{
    public class UpdateVehicleTypeRequest : VehicleTypeRequest
    {
        public int Id { get; set; }
    }
}