﻿namespace Infauto.Services.DTO.Contracts
{
    public class UpdateInsuranceRequest : InsuranceRequest
    {
        public int Id { get; set; }
    }
}