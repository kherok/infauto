﻿namespace Infauto.Services.DTO.Contracts
{
    public class UpdateVehicleRequest : VehicleRequest
    {
        public int Id { get; set; }
    }
}
