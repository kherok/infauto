﻿using System;

namespace Infauto.Services.DTO.Contracts
{
    public class InsuranceRequest
    {
        public string InsuranceFrom { get; set; }
        public string InsuranceTo { get; set; }
        public double InsuranceCost { get; set; }
        public int VehicleId { get; set; }
    }
}