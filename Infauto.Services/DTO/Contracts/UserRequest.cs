﻿namespace Infauto.Services.DTO.Contracts
{
    public class UserRequest
    {
        public string Email { get; set; }
        public string Name { get; set; }
    }
}