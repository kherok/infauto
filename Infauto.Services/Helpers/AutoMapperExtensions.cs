﻿using System;
using System.Collections;
using System.Collections.Generic;
using AutoMapper;

namespace Infauto.Services.Helpers
{
    internal static class AutoMapperExtensions
    {
        public static List<TResult> MapTo<TResult>(this IEnumerable self)
        {
            return (List<TResult>)Mapper.Map(self, self.GetType(), typeof(List<TResult>));
        }

        public static TResult MapTo<TResult>(this object self)
        {
            return (TResult)Mapper.Map(self, self.GetType(), typeof(TResult));
        }

        public static TResult TryMapTo<TResult>(this object self)
        {
            if (self == null)
            {
                return default(TResult);
            }

            return self.MapTo<TResult>();
        }

        public static void MapPropertiesFrom<TSource, TDestination>(this object destination, TSource source)
        {
            if (destination == null)
            {
                throw new ArgumentNullException();
            }

            Mapper.Map(source, (TDestination)destination);
        }
    }
}
