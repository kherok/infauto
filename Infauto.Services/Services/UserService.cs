﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Infauto.DataAccess;
using Infauto.DataAccess.Entities;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;
using Infauto.Services.Helpers;
using Infauto.Services.Interfaces;

namespace Infauto.Services.Services
{
    public class UserService :IUserService
    {
        private readonly IDataDbContext context;

        //public UserService(IDataDbContext context)
        //{
        //    this.context = context;
        //}

        public UserService()
        {
            context = new DataDbContext();
        }

        public async Task<UserDTO> GetUserAsync(string id)
        {
            var result = await context.Users.FirstOrDefaultAsync(x=>x.Email == id);
            return result.MapTo<UserDTO>();
        }

        public int GetUserId(string id)
        {
            var result = context.Users.FirstOrDefault(x => x.Email == id);
            return result.Id;
        }

        public UserDTO Add(UserRequest request)
        {
            var user = new User();
            user.MapPropertiesFrom<UserRequest, User>(request);
            context.Users.Add(user);
            context.SaveChanges();

            return user.MapTo<UserDTO>();
        }

        public UserDTO Update(UpdateUserRequest request)
        {
            var oldUser = context.Users.FirstOrDefaultAsync(x => x.Id == request.Id);
            oldUser.MapPropertiesFrom<UpdateUserRequest, User>(request);
            context.SaveChanges();

            return oldUser.MapTo<UserDTO>();
        }

        public IEnumerable<int> Delete(DeleteRequest request)
        {
            context.Users.RemoveRange(context.Users.Where(x => request.Ids.Contains(x.Id)));
            context.SaveChanges();

            return request.Ids;
        }
    }
}
