﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Infauto.DataAccess;
using Infauto.DataAccess.Entities;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;
using Infauto.Services.Helpers;
using Infauto.Services.Interfaces;

namespace Infauto.Services.Services
{
    public class ServiceService : IServiceService
    {
        private readonly IDataDbContext context;

        //public ServiceService(IDataDbContext context)
        //{
        //    this.context = context;
        //}

        public ServiceService()
        {
            this.context = new DataDbContext();
        }

        public async Task<IEnumerable<ServiceDTO>> GetAllByUserAsync(int id)
        {
            var vehicleIdsList = await context.Vehicles.Where(x => x.UserId == id).Select(x => x.Id).ToListAsync();
            var result = await context.Services.Where(x => vehicleIdsList.Contains(x.VehicleId)).ToListAsync();

            return result.MapTo<ServiceDTO>();
        }

        public async Task<IEnumerable<ServiceDTO>> GetAllByVehicleAsync(int id)
        {
            var result = await context.Services.Where(x => x.VehicleId == id).ToListAsync();

            return result.MapTo<ServiceDTO>();
        }

        public ServiceDTO Add(ServiceRequest request)
        {
            var service = new Service();

            service.MapPropertiesFrom<ServiceRequest, Service>(request);
            context.Services.Add(service);
            context.SaveChanges();

            return service.MapTo<ServiceDTO>();
        }

        public ServiceDTO Update(UpdateServiceRequest request)
        {
            var oldSercive = context.Services.FirstOrDefault(x => x.Id == request.Id);
            oldSercive.MapPropertiesFrom<UpdateServiceRequest, Service>(request);
            context.SaveChanges();

            return oldSercive.MapTo<ServiceDTO>();
        }

        public IEnumerable<int> Delete(DeleteRequest request)
        {
            context.Services.RemoveRange(context.Services.Where(x => request.Ids.Contains(x.Id)));
            context.SaveChanges();

            return request.Ids;
        }
    }
}
