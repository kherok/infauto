﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Infauto.DataAccess;
using Infauto.DataAccess.Entities;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;
using Infauto.Services.Helpers;
using Infauto.Services.Interfaces;

namespace Infauto.Services.Services
{
    public class VehicleTypeService : IVehicleTypeService
    {
        private readonly IDataDbContext context;

        //public VehicleTypeService(IDataDbContext context)
        //{
        //    this.context = context;
        //}

        public VehicleTypeService()
        {
            this.context = new DataDbContext();
        }

        public async Task<IEnumerable<VehicleTypeDTO>> GetAllByUserAsync(int id)
        {
            var result = await context.Vehicles.Where(x => x.UserId == id).Select(x => x.VehicleType).Distinct().ToListAsync();
            return result.MapTo<VehicleTypeDTO>();
        }

        public VehicleTypeDTO Add(VehicleTypeRequest request)
        {
            var newVehicleType = new VehicleType();

            newVehicleType.MapPropertiesFrom<VehicleTypeRequest, VehicleType>(request);
            context.VehicleTypes.Add(newVehicleType);
            context.SaveChanges();

            return newVehicleType.MapTo<VehicleTypeDTO>();
        }

        public VehicleTypeDTO Update(UpdateVehicleTypeRequest request)
        {
            var oldVehicleType = context.VehicleTypes.FirstOrDefault(x => x.Id == request.Id);

            oldVehicleType.MapPropertiesFrom<UpdateVehicleTypeRequest, VehicleType>(request);
            context.SaveChanges();

            return oldVehicleType.MapTo<VehicleTypeDTO>();
        }

        public IEnumerable<int> Delete(DeleteRequest request)
        {
            context.VehicleTypes.RemoveRange(context.VehicleTypes.Where(x => request.Ids.Contains(x.Id)));
            context.SaveChanges();

            return request.Ids;
        }
    }
}
