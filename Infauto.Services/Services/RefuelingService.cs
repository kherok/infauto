﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Infauto.DataAccess;
using Infauto.DataAccess.Entities;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;
using Infauto.Services.Helpers;
using Infauto.Services.Interfaces;

namespace Infauto.Services.Services
{
    public class RefuelingService : IRefuelingService
    {

        private readonly IDataDbContext context;

        //public RefuelingService(IDataDbContext context)
        //{
        //    this.context = context;
        //}

        public RefuelingService()
        {
            this.context = new DataDbContext();
        }

        public async Task<IEnumerable<RefuelingDTO>> GetAllByUserAsync(int id)
        {
            var vehicleIdsList = await context.Vehicles.Where(x => x.UserId == id).Select(x => x.Id).ToListAsync();
            var result = await context.Services.Where(x => vehicleIdsList.Contains(x.VehicleId)).ToListAsync();

            return result.MapTo<RefuelingDTO>();
        }

        public async Task<IEnumerable<RefuelingDTO>> GetAllByVehicleAsync(int id)
        {
            var result = await context.Refuelings.Where(x => x.VehicleId == id).ToListAsync();

            return result.MapTo<RefuelingDTO>();
        }

        public RefuelingDTO Add(RefuelingRequest request)
        {
            var refueling = new Refueling();

            refueling.MapPropertiesFrom<RefuelingRequest, Refueling>(request);
            context.Refuelings.Add(refueling);
            context.SaveChanges();

            return refueling.MapTo<RefuelingDTO>();
        }

        public RefuelingDTO Update(UpdateRefuelingRequest request)
        {
            var oldRefueling = context.Refuelings.FirstOrDefault(x => x.Id == request.Id);
            oldRefueling.MapPropertiesFrom<UpdateRefuelingRequest, Refueling>(request);
            context.SaveChanges();

            return oldRefueling.MapTo<RefuelingDTO>();
        }

        public IEnumerable<int> Delete(DeleteRequest request)
        {
            context.Services.RemoveRange(context.Services.Where(x => request.Ids.Contains(x.Id)));
            context.SaveChanges();

            return request.Ids;
        }
    }
}
