﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Infauto.DataAccess;
using Infauto.DataAccess.Entities;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;
using Infauto.Services.Helpers;
using Infauto.Services.Interfaces;

namespace Infauto.Services.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly IDataDbContext context;

        //public VehicleService(IDataDbContext context)
        //{
        //    this.context = context;
        //}

        public VehicleService()
        {
            this.context = new DataDbContext();
        }

        public async Task<IEnumerable<VehicleDTO>> GetAllByUserAsync(int id)
        {
            var result = await context.Vehicles.Where(x => x.UserId == id).ToListAsync();

            return result.MapTo<VehicleDTO>();
        }

        public VehicleDTO Add(VehicleRequest request, int userId)
        {
            var newVehicle = new Vehicle();

            newVehicle.MapPropertiesFrom<VehicleRequest, Vehicle>(request);
            newVehicle.UserId = userId;
            newVehicle.User = context.Users.FirstOrDefault(x => x.Id == userId);
            context.Vehicles.Add(newVehicle);
            context.SaveChanges();

            return newVehicle.MapTo<VehicleDTO>();
        }

        public VehicleDTO Update(UpdateVehicleRequest request)
        {
            var oldVehicle = context.Vehicles.FirstOrDefault(x => x.Id == request.Id);

            oldVehicle.MapPropertiesFrom<UpdateVehicleRequest, Vehicle>(request);
            context.SaveChanges();

            return oldVehicle.MapTo<VehicleDTO>();
        }

        public IEnumerable<int> Delete(DeleteRequest request)
        {
            context.Vehicles.RemoveRange(context.Vehicles.Where(x => request.Ids.Contains(x.Id)));
            context.SaveChanges();

            return request.Ids;
        }
    }
}
