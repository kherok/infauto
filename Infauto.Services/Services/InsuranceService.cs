﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Infauto.DataAccess;
using Infauto.DataAccess.Entities;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;
using Infauto.Services.Helpers;
using Infauto.Services.Interfaces;

namespace Infauto.Services.Services
{
    public class InsuranceService : IInsuranceService
    {
        private readonly IDataDbContext context;

        //public InsuranceService(IDataDbContext context)
        //{
        //    this.context = context;
        //}

        public InsuranceService()
        {
            context = new DataDbContext();
        }

        public async Task<IEnumerable<InsuranceDTO>> GetAllByUserAsync(int id)
        {
            var vehicleIdsList = await context.Vehicles.Where(x => x.UserId == id).Select(x => x.Id).ToListAsync();
            var result = await context.Insurances.Where(x => vehicleIdsList.Contains(x.Vehicle.Id)).ToListAsync();

            return result.MapTo<InsuranceDTO>();
        }

        public InsuranceDTO Add(InsuranceRequest request)
        {
            var insurance = new Insurance();

            insurance.MapPropertiesFrom<InsuranceRequest, Insurance>(request);
            context.Insurances.Add(insurance);
            context.SaveChanges();

            return insurance.MapTo<InsuranceDTO>();
        }

        public InsuranceDTO Update(UpdateInsuranceRequest request)
        {
            var oldInsurance = context.Insurances.FirstOrDefault(x => x.Id == request.Id);
            oldInsurance.MapPropertiesFrom<UpdateInsuranceRequest, Insurance>(request);
            context.SaveChanges();

            return oldInsurance.MapTo<InsuranceDTO>();
        }

        public IEnumerable<int> Delete(DeleteRequest request)
        {
            context.Insurances.RemoveRange(context.Insurances.Where(x => request.Ids.Contains(x.Id)));
            context.SaveChanges();

            return request.Ids;
        }
    }
}
