﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;

namespace Infauto.Services.Interfaces
{
    public interface IUserService
    {
        Task<UserDTO> GetUserAsync(string id);
        int GetUserId(string id);
        UserDTO Add(UserRequest request);
        UserDTO Update(UpdateUserRequest request);
        IEnumerable<int> Delete(DeleteRequest request);
    }
}
