﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;

namespace Infauto.Services.Interfaces
{
    public interface IServiceService
    {
        Task<IEnumerable<ServiceDTO>> GetAllByUserAsync(int id);
        Task<IEnumerable<ServiceDTO>> GetAllByVehicleAsync(int id);
        ServiceDTO Add(ServiceRequest request);
        ServiceDTO Update(UpdateServiceRequest request);
        IEnumerable<int> Delete(DeleteRequest request);
    }
}
