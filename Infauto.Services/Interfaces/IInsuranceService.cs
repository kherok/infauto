﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infauto.DataAccess.Entities;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;

namespace Infauto.Services.Interfaces
{
    public interface IInsuranceService
    {
        Task<IEnumerable<InsuranceDTO>> GetAllByUserAsync(int id);
        InsuranceDTO Add(InsuranceRequest request);
        InsuranceDTO Update(UpdateInsuranceRequest request);
        IEnumerable<int> Delete(DeleteRequest request);
    }
}
