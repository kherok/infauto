﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;

namespace Infauto.Services.Interfaces
{
    public interface IRefuelingService
    {
        Task<IEnumerable<RefuelingDTO>> GetAllByUserAsync(int id);
        Task<IEnumerable<RefuelingDTO>> GetAllByVehicleAsync(int id);
        RefuelingDTO Add(RefuelingRequest request);
        RefuelingDTO Update(UpdateRefuelingRequest request);
        IEnumerable<int> Delete(DeleteRequest request);
    }
}
