﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;

namespace Infauto.Services.Interfaces
{
    public interface IVehicleService
    {
        Task<IEnumerable<VehicleDTO>> GetAllByUserAsync(int id);
        VehicleDTO Add(VehicleRequest request, int userId);
        VehicleDTO Update(UpdateVehicleRequest request);
        IEnumerable<int> Delete(DeleteRequest request);
    }
}
