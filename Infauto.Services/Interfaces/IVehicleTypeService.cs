﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;

namespace Infauto.Services.Interfaces
{
    public interface IVehicleTypeService
    {
        Task<IEnumerable<VehicleTypeDTO>> GetAllByUserAsync(int id);
        VehicleTypeDTO Add(VehicleTypeRequest request);
        VehicleTypeDTO Update(UpdateVehicleTypeRequest request);
        IEnumerable<int> Delete(DeleteRequest request);
    }
}
