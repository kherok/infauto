﻿using AutoMapper;
using Infauto.DataAccess.Entities;
using Infauto.Services.DTO;
using Infauto.Services.DTO.Contracts;

namespace Infauto.Services
{
    public static class AutoMapperConfig
    {
        public static void CreateMaps()
        {
            Mapper.Initialize(mapper =>
            {
                mapper.CreateMap<Insurance, InsuranceDTO>()
                    .ForMember(x => x.Vehicle, opt => opt.MapFrom(x => x.Vehicle.Name));
                mapper.CreateMap<Refueling, RefuelingDTO>()
                    .ForMember(x => x.Vehicle, opt => opt.MapFrom(x => x.Vehicle.Name));
                mapper.CreateMap<Service, ServiceDTO>()
                    .ForMember(x => x.Vehicle, opt => opt.MapFrom(x => x.Vehicle.Name));
                mapper.CreateMap<User, UserDTO>();
                mapper.CreateMap<Vehicle, VehicleDTO>();
                mapper.CreateMap<VehicleType, VehicleTypeDTO>();

                mapper.CreateMap<InsuranceRequest, Insurance>()
                .ForMember(x => x.Vehicle, opt => opt.Ignore());
                mapper.CreateMap<RefuelingRequest, Refueling>()
                .ForMember(x => x.Vehicle, opt => opt.Ignore());
                mapper.CreateMap<ServiceRequest, Service>()
                .ForMember(x => x.Vehicle, opt => opt.Ignore());
                mapper.CreateMap<UserRequest, User>();
                mapper.CreateMap<VehicleRequest, Vehicle>();
                mapper.CreateMap<VehicleTypeRequest, VehicleType>();

            });
        }
    }
}