﻿(function () {
    'use strict';

    angular.module('app.directives').directive('serviceRow', serviceRow);

    function serviceRow() {
        return {
            restrict: 'E',
            templateUrl: '../../Views/Partials/service-row.html'
        };
    }
})();