﻿(function () {
    'use strict';

    angular.module('app.directives').directive('service', service);

    function service() {
        return {
            restrict: 'E',
            templateUrl: '../../Views/Partials/service.html'
        };
    }
})();