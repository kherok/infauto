﻿(function () {
    'use strict';

    angular.module('app.directives').directive('refueling', refueling);

    function refueling() {
        return {
            restrict: 'E',
            templateUrl: '../../Views/Partials/refueling.html'
        };
    }
})();