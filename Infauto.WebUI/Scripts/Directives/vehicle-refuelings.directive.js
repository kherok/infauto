﻿(function () {
    'use strict';

    angular.module('app.directives').directive('vehicleServices', vehicleServices);

    function vehicleServices() {
        return {
            restrict: 'E',
            templateUrl: '../../Views/Partials/vehicle-services.html'
        };
    }
})();