﻿(function () {
    'use strict';

    angular.module('app.directives').directive('refuelingRow', refuelingRow);

    function refuelingRow() {
        return {
            restrict: 'E',
            templateUrl: '../../Views/Partials/refueling-row.html'
        };
    }
})();