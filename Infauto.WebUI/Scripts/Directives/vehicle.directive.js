﻿(function () {
    'use strict';

    angular.module('app.directives').directive('vehicle', vehicle);

    function vehicle() {
        return {
            restrict: 'E',
            templateUrl: '../../Views/Partials/vehicle.html'
        };
    }
})();