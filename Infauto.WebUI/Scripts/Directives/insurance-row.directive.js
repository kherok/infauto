﻿(function () {
    'use strict';

    angular.module('app.directives').directive('insuranceRow', insuranceRow);

    function insuranceRow() {
        return {
            restrict: 'E',
            templateUrl: '../../Views/Partials/insurance-row.html'
        };
    }
})();