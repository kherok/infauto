﻿(function () {
    'use strict';

    angular.module('app.directives').directive('vehicleRefuelings', vehicleRefuelings);

    function vehicleRefuelings() {
        return {
            restrict: 'E',
            templateUrl: '../../Views/Partials/vehicle-refuelings.html'
        };
    }
})();