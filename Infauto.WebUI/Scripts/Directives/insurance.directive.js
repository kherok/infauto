﻿(function () {
    'use strict';

    angular.module('app.directives').directive('insurance', insurance);

    function insurance() {
        return {
            restrict: 'E',
            templateUrl: '../../Views/Partials/insurance.html'
        };
    }
})();