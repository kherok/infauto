﻿(function () {
    'use strict';

    angular.module('app.directives').directive('vehicleInsurances', vehicleInsurances);

    function vehicleInsurances() {
        return {
            restrict: 'E',
            templateUrl: '../../Views/Partials/vehicle-insurances.html'
        };
    }
})();