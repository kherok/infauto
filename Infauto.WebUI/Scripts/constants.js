﻿(function () {
    angular.module('app')
        .constant('STATES', {
            LOGIN: 'Auth.Login',
            GUEST: 'Guest',
            USER: 'User',
            VEHICLES: 'Vehicles',
            DETAILS: 'Vehicles.Details',
            INSURANCES: 'Vehicles.Insurances',
            SERVICES: 'Vehicles.Services',
            REFUELING: 'Vehicles.Refueling'
        })
        .constant('LANGUAGES', {
            PL: 'PL',
            EN: 'EN'
        })
        .constant('MODALSIZES', {
            LARGE: 'lg',
            SMALL: 'sm'
        })
        .constant('APIURL', 'http://localhost:2582/api/')
        .constant('APIHost', 'http://localhost:2582/');
})();