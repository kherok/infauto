﻿(function () {

    'use strict';

    angular.module('app.controllers').controller('AddVehicleController', AddVehicleController);

    AddVehicleController.$inject = ['$scope', '$modalInstance', 'apiCommunicationService', 'target'];

    function AddVehicleController($scope, $modalInstance, apiCommunicationService, target) {

        var vm = this;

        vm.init = function () {
            vm.target = target === null ? {} : JSON.parse(JSON.stringify(target));
            vm.modalInstance = $modalInstance;
        }

        vm.cancel = function () {
            $modalInstance.dismiss('cancel');
        }

        vm.save = function () {
            if (vm.target.id === undefined) {
                apiCommunicationService.addVehicle(vm.target)
					.then(function (response) {
					    $modalInstance.dismiss('cancel');
					    $scope.$emit('refreshState');
					}, function (response) {
					    var bool = false;
					});
            } else {
                apiCommunicationService.updateVehicle(vm.target)
					.then(function (response) {
					    $modalInstance.dismiss('cancel');
					    $scope.$emit('refreshState');
					}, function (response) {
					    var bool = false;
					});;
            }
        }

        vm.init();
    }
})();