﻿(function () {

    'use strict';

    angular.module('app.controllers').controller('DeleteController', DeleteController);

    DeleteController.$inject = ['$scope', '$modalInstance', 'apiCommunicationService', 'target', 'what'];

    function DeleteController($scope, $modalInstance, apiCommunicationService, target, what) {

        var vm = this;

        vm.init = function () {
            vm.target = target === null ? {} : JSON.parse(JSON.stringify(target));
            vm.modalInstance = $modalInstance;
        }

        vm.cancel = function () {
            $modalInstance.dismiss('cancel');
        }

        vm.delete = function () {
            switch (what) {
                case "service":
                    apiCommunicationService.deleteService(vm.target).then(function (response) {
                        vm.deleted();
                    });
                    break;
                case "refueling":
                    apiCommunicationService.deleteRefueling(vm.target).then(function (response) {
                        vm.deleted();
                    });
                    break;
                case "insurance":
                    apiCommunicationService.deleteInsurance(vm.target).then(function (response) {
                        vm.deleted();
                    });
                    break;
                case "vehicle":
                    apiCommunicationService.deleteVehicle(vm.target).then(function (response) {
                        vm.deleted();
                    });
                    break;
                default:
                    vm.deleted();
            }
        }

        vm.deleted = function () {
            $modalInstance.dismiss('cancel');
            $scope.$emit('refreshState');
        };

        vm.init();
    }
})();