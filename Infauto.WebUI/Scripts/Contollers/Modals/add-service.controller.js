﻿(function () {

    'use strict';

    angular.module('app.controllers').controller('AddServiceController', AddServiceController);

    AddServiceController.$inject = ['$scope', '$modalInstance', 'apiCommunicationService', 'userService', 'target'];

    function AddServiceController($scope, $modalInstance, apiCommunicationService, userService, target) {

        var vm = this;

        vm.init = function () {
            vm.user = {};
            userService.checkForUser().then(function (response) {
                vm.user = response;
            });
            vm.target = target === undefined ? {} : JSON.parse(JSON.stringify(target));
            vm.modalInstance = $modalInstance;
        }

        vm.cancel = function () {
            $modalInstance.dismiss('cancel');
        }

        vm.vehicleSelected = function (vehicle) {
            vm.selectedVehicle = vehicle;
        }

        vm.save = function () {
            if (vm.target.id === undefined) {
                vm.target.vehicleId = vm.selectedVehicle.id;
                apiCommunicationService.addService(vm.target)
					.then(function (response) {
					    $modalInstance.dismiss('cancel');
					    $scope.$emit('refreshState');
					}, function (response) {
					    var bool = false;
					});
            } else {
                apiCommunicationService.updateService(vm.target)
					.then(function (response) {
					    $modalInstance.dismiss('cancel');
					    $scope.$emit('refreshState');
					}, function (response) {
					    var bool = false;
					});;
            }
        }

        vm.init();
    }
})();