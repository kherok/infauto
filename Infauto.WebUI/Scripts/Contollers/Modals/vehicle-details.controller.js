﻿(function () {

    'use strict';

    angular.module('app.controllers').controller('VehicleDetailsController', VehicleDetailsController);

    VehicleDetailsController.$inject = ['$scope', '$modalInstance', 'apiCommunicationService', 'target', 'what'];

    function VehicleDetailsController($scope, $modalInstance, apiCommunicationService, target, what) {

        var vm = this;

        vm.init = function () {
            vm.mode = what;
            vm.showAlert = false;

            var tempTarget = JSON.parse(JSON.stringify(target));
            if (vm.mode === "refuelings") {
                vm.target = tempTarget.refuelings;
            }
            if (vm.mode === "services") {
                vm.target = tempTarget.services;
            }
            if (vm.mode === "insurances") {
                vm.target = tempTarget.insurances;
            }
            if (vm.target.length === 0) {
                vm.showAlert = true;
            }

            vm.modalInstance = $modalInstance;
        }

        vm.cancel = function () {
            $modalInstance.dismiss('cancel');
        }

        vm.init();
    }
})();