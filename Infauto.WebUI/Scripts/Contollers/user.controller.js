﻿(function () {

    angular.module('app.controllers').controller('UserController', UserController);
    UserController.$inject = ['$state', '$scope', 'userService', 'STATES'];

    function UserController($state, $scope, userService, STATES) {

        var vm = this;

        vm.init = function () {
            vm.user = userService.getCurrentUser();
        };

        vm.init();

    };
})();