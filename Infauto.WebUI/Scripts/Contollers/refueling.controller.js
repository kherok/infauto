﻿(function () {

    angular.module('app.controllers').controller('RefuelingController', RefuelingController);
    RefuelingController.$inject = ['modalService', 'userService'];

    function RefuelingController(modalService, userService) {

        var vm = this;

        vm.init = function () {
            userService.checkForUser().then(
                function (response) {
                    vm.user = response;
                });
        };

        vm.addRefueling = function () {
            modalService.addRefueling();
        }

        vm.edit = function (element) {
            modalService.addRefueling(element);
        }

        vm.delete = function (element) {
            modalService.deleteElement(element, "refueling");
        }

        vm.init();

    };
})();