﻿(function () {

    angular.module('app.controllers').controller('InsurancesController', InsurancesController);
    InsurancesController.$inject = ['modalService', 'userService'];

    function InsurancesController(modalService, userService) {

        var vm = this;

        vm.init = function () {
            userService.checkForUser().then(
                function (response) {
                    vm.user = response;
                });
        };

        vm.addIsurance = function () {
            modalService.addInsurance();
        }

        vm.edit = function (element) {
            modalService.addInsurance(element);
        }

        vm.delete = function (element) {
            modalService.deleteElement(element, "insurance");
        }

        vm.init();

    };
})();