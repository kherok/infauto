﻿(function () {

    angular.module('app.controllers').controller('ServicesController', ServicesController);
    ServicesController.$inject = ['modalService', 'userService'];

    function ServicesController(modalService, userService) {

        var vm = this;

        vm.init = function () {
            userService.checkForUser().then(
                function (response) {
                    vm.user = response;
                });
        };

        vm.addService = function () {
            modalService.addService();
        }

        vm.editService=function(element) {
            modalService.addService(element);
        }

        vm.deleteService = function (element) {
            modalService.deleteElement(element, "service");
        }

        vm.init();

    };
})();