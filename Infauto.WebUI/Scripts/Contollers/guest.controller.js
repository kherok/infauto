﻿(function () {

    angular.module('app.controllers').controller('GuestController', GuestController);
    GuestController.$inject = ['$scope', 'userService'];

    function GuestController($scope, userService) {

        var vm = this;
        vm.showAlert = false;

        vm.init = function () {
        };

        vm.login = function () {
            vm.userLogin = {
                email: vm.email,
                password: vm.password
            }
            userService.login(vm.userLogin).then(function(response) {
                vm.user = response;
                vm.showAlert = false;
                $scope.$emit('userLogedIn', vm.user);
            }, function(response) {
                vm.showAlert = true;
            });
        }

        vm.register = function () {
            vm.userRegister = {
                name: vm.registerUserName,
                email: vm.registerUserEmail,
                password: vm.registerPassword,
                confirm: vm.registerConfirm
            }

            userService.register(vm.userRegister).then(function (response) {
                vm.user = response;
                $scope.$emit('userLogedIn', vm.user);
            }, function (response) {
                //todo fail
            });
        }

        vm.init();
    };
})();