﻿(function () {
    angular.module('app.controllers').controller('IndexController', IndexController);

    IndexController.$inject = ['$scope', '$rootScope', '$state', 'apiCommunicationService', 'languageService', 'userService', 'LANGUAGES', 'STATES'];

    function IndexController($scope, $rootScope, $state, apiCommunicationService, languageService, userService, LANGUAGES, STATES) {

        var vm = this;
        vm.viewLanguageConst = LANGUAGES;
        vm.defaultUserName = "Wellcome Guest";

        vm.init = function () {
            vm.language = languageService.getLanguage();
            vm.isUserLoggedIn = userService.isUserLogged();
            vm.language = languageService.getLanguage();
            vm.states = STATES;

            if (vm.isUserLoggedIn) {
                vm.user = userService.getCurrentUser();
                vm.userLoggedIn(vm.user);
            } else {
                vm.user = {}
                vm.user.name = vm.defaultUserName;
            }
        };

        vm.logOut = function () {
            userService.logOut().then(function(response) {
                vm.isUserLoggedIn = userService.isUserLogged();
                vm.user = {};
                vm.user.name = vm.defaultUserName;
                vm.menuNavigate(vm.states.GUEST);
            });
        };

        vm.languageChanged = function (language) {
            vm.language = language;
            languageService.setLanguage(language);
        };

        vm.switchToEnglish = function () {
            vm.language = vm.viewLanguageConst.EN;
            languageService.setLanguage(vm.language);
        };

        vm.switchToPolish = function () {
            vm.language = vm.viewLanguageConst.PL;
            languageService.setLanguage(vm.language);
        };

        vm.menuNavigate = function (state) {
            $state.go(state);
        };

        vm.userLoggedIn = function (user) {
            vm.isUserLoggedIn = true;
            vm.user = user;
            $state.go(vm.states.VEHICLES);
        };

        $scope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                vm.activeState = toState.name;
            });

        $scope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                if (toState.name !== vm.states.GUEST && !vm.isUserLoggedIn) {
                    event.preventDefault();
                    $state.go(vm.states.GUEST);
                } else if (toState.name === vm.states.GUEST && vm.isUserLoggedIn) {
                    event.preventDefault();
                    $state.go(vm.states.VEHICLES);
                }
            });

        $rootScope.$on('userLogedIn',
            function (event, user) {
                vm.userLoggedIn(user);
            });

        $rootScope.$on('refreshState',
            function () {
                $state.reload();
            });


        vm.setTimeRange = function() {
            var cos = false;
        };

        vm.init();
    };
})();