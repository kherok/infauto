﻿(function () {

    angular.module('app.controllers').controller('VehiclesController', VehiclesController);

    VehiclesController.$inject = ['userService', 'modalService'];

    function VehiclesController(userService, modalService) {

        var vm = this;
        vm.user = {};

        vm.init = function () {
            userService.checkForUser().then(
                function (response) {
                    vm.user = response;
                });
        };

        vm.addVehicle = function () {
            modalService.addVehicle();
        };

        vm.openModal = function (event, vehicle) {
            if (event.target.localName !== "button")
                modalService.editVehicle(vehicle);
        };

        vm.refueling = function (event, vehicle) {
            modalService.showVehicleRefuelings(vehicle);
        }

        vm.services = function (event, vehicle) {
            modalService.showVehicleServices(vehicle);
        }

        vm.insurances = function (event, vehicle) {
            modalService.showVehicleInsurances(vehicle);
        }

        vm.delete = function(event, vehicle) {
            modalService.deleteElement(vehicle, "vehicle");
        }

        vm.init();

    };
})();