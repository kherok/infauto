﻿(function () {

    'use strict';

    angular.module('app.services').factory('languageService', languageService);

    languageService.$inject = ['$rootScope', '$translate', 'localStorageService', 'LANGUAGES'];

    function languageService($rootScope, $translate, localStorageService, LANGUAGES) {

        function getLanguage() {
            var language = localStorageService.cookie.get('language');

            if (!language) {
                language = LANGUAGES.EN;
                setLanguage(language);
            }
            return language;
        }

        function setLanguage(language) {
            localStorageService.cookie.set('language', language);
            $translate.use(language);
        }

        function init() {
            var language = getLanguage();
            $translate.use(language);
        }

        init();

        return {
            getLanguage: getLanguage,
            setLanguage: setLanguage
        };
    }
})();