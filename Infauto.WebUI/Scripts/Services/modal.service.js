﻿(function () {

    'use strict';

    angular.module('app.services').factory('modalService', modalService);

    modalService.$inject = ['$modal', 'MODALSIZES'];

    function modalService($modal, MODALSIZES) {

        var vm = this;

        vm.isModalOpened = false;

        function show(target, html, controller, size) {
            vm.isModalOpened = true;
            vm.modalInstance = $modal.open({
                animation: true,
                templateUrl: '../../Views/Modals/' + html,
                controller: controller + ' as vm',
                size: size,
                resolve: {
                    target: function () {
                        return target;
                    }
                }
            });
        };

        function showSpecial(target, html, controller, size, what) {
            vm.modalInstance = $modal.open({
                animation: true,
                templateUrl: '../../Views/Modals/' + html,
                controller: controller + ' as vm',
                size: size,
                resolve: {
                    target: function () {
                        return target;
                    },
                    what: function () {
                        return what;
                    }
                }
            });
        };

        var addVehicle = function () {
            show(null, 'add-vehicle.html', 'AddVehicleController', MODALSIZES.LARGE);
        };

        var editVehicle = function (target) {
            show(target, 'add-vehicle.html', 'AddVehicleController', MODALSIZES.LARGE);
        };

        var showVehicleRefuelings = function (target) {
            showSpecial(target, 'vehicle-details.html', 'VehicleDetailsController', MODALSIZES.LARGE, "refuelings");
        };

        var showVehicleServices = function (target) {
            showSpecial(target, 'vehicle-details.html', 'VehicleDetailsController', MODALSIZES.LARGE, "services");
        };

        var showVehicleInsurances = function (target) {
            showSpecial(target, 'vehicle-details.html', 'VehicleDetailsController', MODALSIZES.LARGE, "insurances");
        };

        var addInsurance = function (element) {
            show(element, 'add-insurance.html', 'AddInsuranceController', MODALSIZES.LARGE);
        };

        var addRefueling = function (element) {
            show(element, 'add-refueling.html', 'AddRefuelingController', MODALSIZES.LARGE);
        };

        var addService = function (element) {
            show(element, 'add-service.html', 'AddServiceController', MODALSIZES.LARGE);
        };

        var deleteElement = function(target, targetType) {
            showSpecial(target, 'delete.html', 'DeleteController', MODALSIZES.SMALL, targetType);
        };

        return {
            addVehicle: addVehicle,
            editVehicle: editVehicle,
            showVehicleRefuelings: showVehicleRefuelings,
            showVehicleServices: showVehicleServices,
            showVehicleInsurances: showVehicleInsurances,
            addInsurance: addInsurance,
            addRefueling: addRefueling,
            addService: addService,
            deleteElement: deleteElement
        };
    }
})();