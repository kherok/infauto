﻿(function () {

    'use strict';

    angular.module('app.services').factory('userService', userService);

    userService.$inject = ['$q', 'localStorageService', 'apiCommunicationService'];

    function userService($q, localStorageService, apiCommunicationService) {

        var me = this;
        me.isRequestInProgress = false;
        me.userLocalstorageKey = 'cookedUser';

        function getCurrentUser() {
            return me.currentUser;
        };

        function checkForUser() {
            var deferred = $q.defer();

            if (me.isRequestInProgress) {
                return me.stashDeferred.promise;
            } else {
                me.cookedUser = localStorageService.get(me.userLocalstorageKey);
                if (!me.cookedUser) {
                    return null;
                }
                me.isRequestInProgress = true;
                me.stashDeferred = deferred;
                apiCommunicationService.getUser(me.cookedUser.email)
                    .then(function (response) {
                        me.isRequestInProgress = false;
                        me.currentUser = response.data;
                        deferred.resolve(me.currentUser);
                        me.stashDeferred.resolve(me.currentUser);
                    }, function (response) {
                        me.isRequestInProgress = false;
                        return deferred.reject(response.data);
                    });
            }

            return deferred.promise;
        };

        function logOut() {
            var deferred = $q.defer();

            apiCommunicationService.logOut().then(function(response) {
                if (response.status === 200) {
                    localStorageService.remove(me.userLocalstorageKey);
                    me.currentUser = null;
                    deferred.resolve();
                }
            });

            return deferred.promise;
        }

        function isUserLogged() {
            return me.currentUser === null ? false : true;
        }

        function putUserInCiastko(userCiastko) {
            me.currentUser = userCiastko;
            localStorageService.set(me.userLocalstorageKey, me.currentUser);
        }

        function login(userData) {
            var deferred = $q.defer();
            apiCommunicationService.login(userData).then(function (response) {
                putUserInCiastko(response.data);
                return deferred.resolve(response.data);
            }, function (response) {
                return deferred.reject(response);
            });

            return deferred.promise;
        }

        function register(userRegister) {
            var deferred = $q.defer();
            apiCommunicationService.register(userRegister).then(function (response) {
                if (response.status !== 200) {
                    return deferred.reject(response.data);
                }
                putUserInCiastko(response.data);
                return deferred.resolve(response.data);
            }, function (response) {
                //todo Co jeśli error
                return deferred.reject(response);
            });

            return deferred.promise;
        }

        function init() {
            me.currentUser = localStorageService.get(me.userLocalstorageKey);
        };

        init();

        return {
            getCurrentUser: getCurrentUser,
            checkForUser: checkForUser,
            logOut: logOut,
            login: login,
            isUserLogged: isUserLogged,
            register: register
        };
    }
})();