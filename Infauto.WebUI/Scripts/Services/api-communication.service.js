﻿(function () {

    'use strict';

    angular.module('app.services').factory('apiCommunicationService', apiCommunicationService);

    apiCommunicationService.$inject = ['$q', '$http', 'APIURL', 'APIHost'];

    function apiCommunicationService($q, $http, APIURL, APIHost) {
        var apiUri = APIURL;

        function callApi(controller, action, data) {
            var uri = apiUri + controller + '/' + action;
            return $http.post(uri, data);
        };

        function getUser(id) {
            return $http.get(apiUri + 'user/GetUser/' + id);
        };

        function getMagicString(userData) {
            var data = "grant_type=password&username=" + userData.email + "&password=" + userData.password;
            return $http.post(APIHost + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } });
        }

        function generateError(result, response) {
            result.succes = false;
            var errors = [];
            for (var key in response.data.modelState) {
                for (var i = 0; i < response.data.modelState[key].length; i++) {
                    errors.push(response.data.modelState[key][i]);
                }
            }
            result.data = "Failed to register user due to:" + errors.join(' ');
            return result;
        }

        function register(userRegister) {
            var deferred = $q.defer();
            var data = {
                email: userRegister.email,
                password: userRegister.password,
                confirmPassword: userRegister.confirm
            }
            var result = {
                data: {}
            };

            $http.post(
                    apiUri + 'Account/Register',
                    JSON.stringify(data))
                .then(function (response) {
                    result.succes = true;
                    getMagicString(data).then(function (respose2) {
                        result.data.magicString = respose2.data.access_token;
                        data.name = userRegister.name;
                        $http.post(
                                apiUri + 'User/Add',
                                JSON.stringify(data),
                                { headers: { 'Authorization': 'Bearer ' + result.data.magicString } })
                            .then(function (response3) {
                                result.data.id = response3.data.id;
                                result.data.name = response3.data.name;
                                result.data.email = response3.data.email;
                                result.data.vehicles = response3.data.vehicles;
                                response3.data = result.data;
                                return deferred.resolve(response3);
                            }, function (response3) {
                                return deferred.reject(generateError(result, response3));
                            });
                    }, function (respose2) {

                    });
                }, function (response) {
                    return deferred.reject(generateError(result, response));
                });

            return deferred.promise;
        }

        function login(userLogin) {
            var deferred = $q.defer();
            var result = {
                data: {}
            };
            getMagicString(userLogin).then(function (response) {
                result.data.magicString = response.data.access_token;
                result.data.email = response.data.userName;
                $http.get(
                        apiUri + 'User/GetUser/' + result.data.email,
                        { headers: { 'Authorization': 'Bearer ' + result.data.magicString } })
                    .then(function (response3) {
                        response3.data.magicString = result.data.magicString;
                        return deferred.resolve(response3);
                    }, function (response3) {
                        return deferred.reject(generateError(result, response3));
                    });
            }, function (response) {
                return deferred.reject(response);
            });

            return deferred.promise;
        }

        function logOut() {
            return $http.post(apiUri + 'Account/Logout');
        }

        function addVehicle(data) {
            return $http.post(apiUri + 'Vehicle/Add', data);
        }

        function updateVehicle(data) {
            return $http.post(apiUri + 'Vehicle/Update', data);
        }

        function addRefueling(data) {
            return $http.post(apiUri + 'Refueling/Add', data);
        }

        function updateRefueling(data) {
            return $http.post(apiUri + 'Refueling/Update', data);
        }

        function addService(data) {
            return $http.post(apiUri + 'Service/Add', data);
        }

        function updateService(data) {
            return $http.post(apiUri + 'Service/Update', data);
        }

        function addInsurance(data) {
            return $http.post(apiUri + 'Insurance/Add', data);
        }

        function updateInsurance(data) {
            return $http.post(apiUri + 'Insurance/Update', data);
        }

        function deleteService(data) {
            var array = [];
            array.push(data.id);
            return $http.post(apiUri + 'Service/Delete', { ids: array });
        }

        function deleteRefueling(data) {
            var array = [];
            array.push(data.id);
            return $http.post(apiUri + 'Refueling/Delete', { ids: array });
        }

        function deleteInsurance(data) {
            var array = [];
            array.push(data.id);
            return $http.post(apiUri + 'Insurance/Delete', { ids: array });
        }
        
        function deleteVehicle(data) {
            var array = [];
            array.push(data.id);
            return $http.post(apiUri + 'Vehicle/Delete', { ids: array });
        }

        return {
            getUser: getUser,
            getMagicString: getMagicString,
            register: register,
            login: login,
            logOut: logOut,
            addVehicle: addVehicle,
            updateVehicle: updateVehicle,
            addRefueling: addRefueling,
            updateRefueling: updateRefueling,
            addService: addService,
            updateService: updateService,
            addInsurance: addInsurance,
            updateInsurance: updateInsurance,
            deleteService: deleteService,
            deleteRefueling: deleteRefueling,
            deleteInsurance: deleteInsurance,
            deleteVehicle: deleteVehicle
        };
    }
})();