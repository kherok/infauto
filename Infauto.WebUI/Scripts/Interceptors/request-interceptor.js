﻿(function () {

    'use strict';

    angular.module('app.services').factory('requestInterceptor', requestInterceptor);

    requestInterceptor.$inject = ['$q', 'localStorageService', 'STATES'];

    function requestInterceptor($q, localStorageService, STATES) {
        var authInterceptorServiceFactory = {};

        var request = function (config) {

            config.headers = config.headers || {};

            var authData = localStorageService.get('cookedUser');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.magicString;
            }

            return config;
        }

        var responseError = function (rejection) {
            if (rejection.status === 401) {
            }
            return $q.reject(rejection);
        }

        authInterceptorServiceFactory.request = request;
        authInterceptorServiceFactory.responseError = responseError;

        return authInterceptorServiceFactory;
    }
})();
