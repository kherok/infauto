﻿(function () {

    'use strict';

    angular.module('app.services').factory('responseInterceptor', responseInterceptor);

    responseInterceptor.$inject = [];

    function responseInterceptor() {

        var notifyUserAboutSuccess = function (response) {
            var cos = "dupa0";
        };

        return {
            response: function (response) {
                if (response.config.method === 'POST') {
                    notifyUserAboutSuccess(response);
                }
                return response;
            }
        };
    }
})();
