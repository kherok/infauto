﻿(function () {
    angular.module('app',
        ['app.controllers',
         'app.directives',
         'app.services',
         'ui.router',
         'ui.bootstrap',
         'pascalprecht.translate',
         'LocalStorageModule',
         'ngAnimate'
         ]);
})();
