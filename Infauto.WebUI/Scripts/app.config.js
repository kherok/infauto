﻿(function () {

    'use strict';

    angular.module('app').config(configure);

    function configure($stateProvider, $httpProvider, $urlRouterProvider, STATES, $translateProvider) {
        $stateProvider
            .state(STATES.LOGIN, {
                url: '/Login',
                views: {
                    'main@': {
                        templateUrl: '../Views/login.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state(STATES.USER, {
                url: '/User',
                views: {
                    'main@': {
                        templateUrl: '../Views/user.html',
                        controller: 'UserController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state(STATES.VEHICLES, {
                url: '/Vehicles',
                views: {
                    'main@': {
                        templateUrl: '../Views/vehicles.html',
                        controller: 'VehiclesController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state(STATES.DETAILS, {
                url: '/Vehicles/Details/:VehicleId',
                views: {
                    'main@': {
                        templateUrl: '../Views/details.html',
                        controller: 'DetailsController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    VehicleId: {
                        value: null,
                        squash: false
                    }
                }
            })
            .state(STATES.GUEST, {
                url: '/Guest',
                views: {
                    'main@': {
                        templateUrl: '../Views/guest.html',
                        controller: 'GuestController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state(STATES.INSURANCES, {
                url: '/Insurances',
                views: {
                    'main@': {
                        templateUrl: '../Views/insurances.html',
                        controller: 'InsurancesController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state(STATES.SERVICES, {
                url: '/Services',
                views: {
                    'main@': {
                        templateUrl: '../Views/services.html',
                        controller: 'ServicesController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state(STATES.REFUELING, {
                url: '/Refueling',
                views: {
                    'main@': {
                        templateUrl: '../Views/refueling.html',
                        controller: 'RefuelingController',
                        controllerAs: 'vm'
                    }
                }
            });

        $urlRouterProvider.otherwise('/Vehicles');
        $httpProvider.interceptors.push('requestInterceptor');
        configureAngularTranslate($translateProvider);
    }

    function configureAngularTranslate($translateProvider) {
        $translateProvider
            .preferredLanguage('en')
            .useSanitizeValueStrategy('sanitizeParameters')
            .useStaticFilesLoader({
                prefix: '../Resources/Translations/locale-',
                suffix: '.json'
            });
    }
})();