﻿using System.Data.Entity;
using Infauto.DataAccess.Configurations;
using Infauto.DataAccess.Entities;

namespace Infauto.DataAccess
{
    public class DataDbContext : DbContext, IDataDbContext
    {
        public DataDbContext()
            : base("DataConnection")
        {
        }

        public DbSet<Insurance> Insurances { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Refueling> Refuelings { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<VehicleType> VehicleTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new InsuranceConfiguration());
            modelBuilder.Configurations.Add(new RefuelingConfiguration());
            modelBuilder.Configurations.Add(new ServiceConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new VehicleConfiguration());
            modelBuilder.Configurations.Add(new VehicleTypeConfiguration());
        }
    }
}
