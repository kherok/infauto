﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Infauto.DataAccess.Entities;

namespace Infauto.DataAccess
{
    public interface IDataDbContext : IDisposable, IObjectContextAdapter
    {
        DbSet<Insurance> Insurances { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<Refueling> Refuelings { get; set; }
        DbSet<Service> Services{ get; set; }
        DbSet<Vehicle> Vehicles { get; set; }
        DbSet<VehicleType> VehicleTypes { get; set; }
        int SaveChanges();
    }
}
