namespace Infauto.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VehicleIdAddedToInsurance : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Insurances", name: "Vehicle_Id", newName: "VehicleId");
            RenameIndex(table: "dbo.Insurances", name: "IX_Vehicle_Id", newName: "IX_VehicleId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Insurances", name: "IX_VehicleId", newName: "IX_Vehicle_Id");
            RenameColumn(table: "dbo.Insurances", name: "VehicleId", newName: "Vehicle_Id");
        }
    }
}
