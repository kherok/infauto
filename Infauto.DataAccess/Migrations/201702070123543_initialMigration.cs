namespace Infauto.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Insurances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InsuranceFrom = c.String(),
                        InsuranceTo = c.String(),
                        InsuranceCost = c.Double(nullable: false),
                        Vehicle_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicles", t => t.Vehicle_Id)
                .Index(t => t.Vehicle_Id);
            
            CreateTable(
                "dbo.Vehicles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Mileage = c.Int(nullable: false),
                        HorsePower = c.Int(nullable: false),
                        ProductionYear = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        VehicleTypeId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Refuelings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MileageDifference = c.Int(nullable: false),
                        FuelCost = c.Double(nullable: false),
                        VehicleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicles", t => t.VehicleId)
                .Index(t => t.VehicleId);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ServiceDescription = c.String(),
                        ServiceCost = c.Double(nullable: false),
                        Mileage = c.Int(nullable: false),
                        IsCyclic = c.Boolean(nullable: false),
                        MileageCycle = c.Int(),
                        VehicleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicles", t => t.VehicleId)
                .Index(t => t.VehicleId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VehicleTypes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Type = c.String(),
                        FuelType = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicles", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Insurances", "Vehicle_Id", "dbo.Vehicles");
            DropForeignKey("dbo.VehicleTypes", "Id", "dbo.Vehicles");
            DropForeignKey("dbo.Vehicles", "UserId", "dbo.Users");
            DropForeignKey("dbo.Services", "VehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.Refuelings", "VehicleId", "dbo.Vehicles");
            DropIndex("dbo.VehicleTypes", new[] { "Id" });
            DropIndex("dbo.Services", new[] { "VehicleId" });
            DropIndex("dbo.Refuelings", new[] { "VehicleId" });
            DropIndex("dbo.Vehicles", new[] { "UserId" });
            DropIndex("dbo.Insurances", new[] { "Vehicle_Id" });
            DropTable("dbo.VehicleTypes");
            DropTable("dbo.Users");
            DropTable("dbo.Services");
            DropTable("dbo.Refuelings");
            DropTable("dbo.Vehicles");
            DropTable("dbo.Insurances");
        }
    }
}
