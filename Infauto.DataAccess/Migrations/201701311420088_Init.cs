namespace Infauto.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Insurances",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        InsuranceFrom = c.DateTime(nullable: false),
                        InsuranceTo = c.DateTime(nullable: false),
                        InsuranceCost = c.Double(nullable: false),
                        VehicleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicles", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Vehicles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Mileage = c.Int(nullable: false),
                        HorsePower = c.Int(nullable: false),
                        ProductionYear = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        VehicleTypeId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Refuelings",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        MileageDifference = c.Int(nullable: false),
                        FuelCost = c.Double(nullable: false),
                        VehicleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicles", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ServiceDescription = c.String(),
                        ServiceCost = c.Double(nullable: false),
                        Mileage = c.Int(nullable: false),
                        IsCyclic = c.Boolean(nullable: false),
                        MileageCycle = c.Int(),
                        VehicleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicles", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VehicleTypes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Type = c.String(),
                        FuelType = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicles", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Insurances", "Id", "dbo.Vehicles");
            DropForeignKey("dbo.VehicleTypes", "Id", "dbo.Vehicles");
            DropForeignKey("dbo.Vehicles", "UserId", "dbo.Users");
            DropForeignKey("dbo.Services", "Id", "dbo.Vehicles");
            DropForeignKey("dbo.Refuelings", "Id", "dbo.Vehicles");
            DropIndex("dbo.VehicleTypes", new[] { "Id" });
            DropIndex("dbo.Services", new[] { "Id" });
            DropIndex("dbo.Refuelings", new[] { "Id" });
            DropIndex("dbo.Vehicles", new[] { "UserId" });
            DropIndex("dbo.Insurances", new[] { "Id" });
            DropTable("dbo.VehicleTypes");
            DropTable("dbo.Users");
            DropTable("dbo.Services");
            DropTable("dbo.Refuelings");
            DropTable("dbo.Vehicles");
            DropTable("dbo.Insurances");
        }
    }
}
