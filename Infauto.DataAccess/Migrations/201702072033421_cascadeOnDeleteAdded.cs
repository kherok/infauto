namespace Infauto.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cascadeOnDeleteAdded : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Insurances", "VehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.Refuelings", "VehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.Services", "VehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.Vehicles", "UserId", "dbo.Users");
            AddForeignKey("dbo.Insurances", "VehicleId", "dbo.Vehicles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Refuelings", "VehicleId", "dbo.Vehicles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Services", "VehicleId", "dbo.Vehicles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Vehicles", "UserId", "dbo.Users", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Vehicles", "UserId", "dbo.Users");
            DropForeignKey("dbo.Services", "VehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.Refuelings", "VehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.Insurances", "VehicleId", "dbo.Vehicles");
            AddForeignKey("dbo.Vehicles", "UserId", "dbo.Users", "Id");
            AddForeignKey("dbo.Services", "VehicleId", "dbo.Vehicles", "Id");
            AddForeignKey("dbo.Refuelings", "VehicleId", "dbo.Vehicles", "Id");
            AddForeignKey("dbo.Insurances", "VehicleId", "dbo.Vehicles", "Id");
        }
    }
}
