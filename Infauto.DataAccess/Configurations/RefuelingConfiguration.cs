﻿using System.Data.Entity.ModelConfiguration;
using Infauto.DataAccess.Entities;

namespace Infauto.DataAccess.Configurations
{
    class RefuelingConfiguration :EntityTypeConfiguration<Refueling>
    {
        public RefuelingConfiguration()
        {
            this.HasRequired(x => x.Vehicle)
                .WithMany(v => v.Refuelings)
                .WillCascadeOnDelete(true);
        }
    }
}
