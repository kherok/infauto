﻿using System.Data.Entity.ModelConfiguration;
using Infauto.DataAccess.Entities;

namespace Infauto.DataAccess.Configurations
{
    class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            this.HasOptional(x => x.Vehicles);
        }
    }
}
