﻿using System.Data.Entity.ModelConfiguration;
using Infauto.DataAccess.Entities;

namespace Infauto.DataAccess.Configurations
{
    class VehicleTypeConfiguration : EntityTypeConfiguration<VehicleType>
    {
        public VehicleTypeConfiguration()
        {
            this.HasRequired(x => x.Vehicles);
        }
    }
}
