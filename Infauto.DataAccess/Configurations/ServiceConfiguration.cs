﻿using System.Data.Entity.ModelConfiguration;
using Infauto.DataAccess.Entities;

namespace Infauto.DataAccess.Configurations
{
    class ServiceConfiguration : EntityTypeConfiguration<Service>
    {
        public ServiceConfiguration()
        {
            this.HasRequired(x => x.Vehicle)
                .WithMany(v => v.Services)
                .WillCascadeOnDelete(true);
        }
    }
}
