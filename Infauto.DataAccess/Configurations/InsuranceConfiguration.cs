﻿using System.Data.Entity.ModelConfiguration;
using Infauto.DataAccess.Entities;

namespace Infauto.DataAccess.Configurations
{
    public class InsuranceConfiguration : EntityTypeConfiguration<Insurance>
    {
        public InsuranceConfiguration()
        {
            this.HasRequired(x => x.Vehicle)
                .WithMany(u => u.Insurances)
                .WillCascadeOnDelete(true);
        }
    }
}