﻿using System.Data.Entity.ModelConfiguration;
using Infauto.DataAccess.Entities;

namespace Infauto.DataAccess.Configurations
{
    class VehicleConfiguration : EntityTypeConfiguration<Vehicle>
    {
        public VehicleConfiguration()
        {
            this.HasRequired(x => x.User)
                .WithMany(u=>u.Vehicles)
                .WillCascadeOnDelete(true);
        }
    }
}
