﻿using System.Collections.Generic;

namespace Infauto.DataAccess.Entities
{
    public class VehicleType
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string FuelType { get; set; }
        public virtual ICollection<Vehicle> Vehicles { get; set; } 
    }
}