﻿using System;

namespace Infauto.DataAccess.Entities
{
    public class Insurance
    {
        public int Id { get; set; }
        public string InsuranceFrom { get; set; }
        public string InsuranceTo { get; set; }
        public double InsuranceCost { get; set; }
        public int VehicleId { get; set; }
        public virtual Vehicle Vehicle { get; set; }
    }
}