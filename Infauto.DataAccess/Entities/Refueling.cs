﻿namespace Infauto.DataAccess.Entities
{
    public class Refueling
    {
        public int Id { get; set; }
        public int MileageDifference { get; set; }
        public double FuelCost { get; set; }
        public int VehicleId { get; set; }
        public virtual Vehicle Vehicle { get; set; }
    }
}