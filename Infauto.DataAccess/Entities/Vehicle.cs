﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infauto.DataAccess.Entities
{
    public class Vehicle
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Mileage { get; set; }
        public int HorsePower { get; set; }
        public int ProductionYear { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int? VehicleTypeId { get; set; }
        public virtual VehicleType VehicleType { get; set; }
        public virtual ICollection<Refueling> Refuelings { get; set; }
        public virtual ICollection<Insurance> Insurances { get; set; }
        public virtual ICollection<Service> Services { get; set; }
    }
}